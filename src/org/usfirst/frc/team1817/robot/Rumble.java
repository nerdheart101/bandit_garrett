package org.usfirst.frc.team1817.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Timer;

public class Rumble implements Runnable{
    GenericHID stick;
    double endTime;
    Thread t;
    
    public Rumble(GenericHID stick){
        this.stick = stick;
        endTime = Timer.getFPGATimestamp();
        
        t = new Thread(this, String.valueOf(Math.random()));
        t.start();
    }
    
    public void rumbleForTime(double seconds){
        endTime = Timer.getFPGATimestamp() + seconds;
    }
    
    public void run(){
        while(!t.interrupted()){
            double time = Timer.getFPGATimestamp();
            if(endTime > time){
                stick.setRumble(GenericHID.RumbleType.kLeftRumble, 1.0);
                stick.setRumble(GenericHID.RumbleType.kRightRumble, 1.0);
            } else {
                stick.setRumble(GenericHID.RumbleType.kLeftRumble, 0.0);
                stick.setRumble(GenericHID.RumbleType.kRightRumble, 0.0);
            }
            
            Timer.delay(0.005);
        }
    }
}
