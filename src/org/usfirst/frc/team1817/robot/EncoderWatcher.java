package org.usfirst.frc.team1817.robot;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class EncoderWatcher implements Runnable{
    Encoder encoder;
    String name;
    Thread t;
    
    public EncoderWatcher(Encoder encoder, String name){
        this.encoder = encoder;
        this.name = name;
        
        t = new Thread(this, name);
        t.start();
    }
    
    public void run(){
        while(!t.interrupted()){
            SmartDashboard.putNumber(name.concat(" Value"), encoder.getDistance());
            SmartDashboard.putNumber(name.concat(" Rate"), encoder.getRate());
            
            Timer.delay(0.005);
        }
    }
}
