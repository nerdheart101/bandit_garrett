package org.usfirst.frc.team1817.robot;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class GearPOT implements Runnable {
    Thread t;
    PWMSpeedController gearM;
    AnalogInput gearPot;
    int state;
    double min, max, score;
    
    public GearPOT(PWMSpeedController gearM, AnalogInput pot, double min, double max, double score){
        this.gearM = gearM;
        this.gearPot = pot;
        this.state = 0;
        this.min = min;
        this.max = max;
        this.score = score;
        
        t = new Thread(this, "Blah");
        t.start();
    }
    
    public double getSpeed(){
        double speed = 0.0;
        switch(state){
            case 3: // Score
                speed = this.score - gearPot.pidGet();
                break;
            case 2: // Up
                speed = this.max - gearPot.pidGet();
                break;
            case 1: // Down
                speed = this.min - gearPot.pidGet();
                break;
            case 0: // Disabled
            default: break;
        }
        
        return speed;
    }
    
    public void score(){
        state = 3;
    }
    
    public void up(){
        state = 2;
    }
    
    public void down(){
        state = 1;
    }
    
    public void disable(){
        state = 0;
    }
    
    public void run(){
        while(!t.interrupted()){
            SmartDashboard.putNumber("Gear POT", gearPot.pidGet());
            double maxSpeed = 0.25;
            double speed = -Math.min(Math.max(this.getSpeed() / 4.0, -maxSpeed), maxSpeed);
            gearM.set(speed);
            
            Timer.delay(0.005);
        }
    }
}
