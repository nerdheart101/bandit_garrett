package org.usfirst.frc.team1817.robot;

import edu.wpi.cscore.*;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.opencv.core.*;
import org.opencv.imgproc.*;

public class RobotCam implements Runnable {
    private UsbCamera cam;
    private Thread t;
    
    public RobotCam(){
        cam = CameraServer.getInstance().startAutomaticCapture("cam0", 0);
        cam.setResolution(320, 240);
        cam.setFPS(30);
        
        SmartDashboard.putNumber("Brightness", 0);
        
        t = new Thread(this, "Cam");
        t.start();
    }
    
    public void run(){
        CvSink input = CameraServer.getInstance().getVideo();
        CvSource output = CameraServer.getInstance().putVideo("Flipped", 320, 240);
        
        Mat src  = new Mat();
        Mat dst  = new Mat();
        
        while(!t.interrupted()){
            cam.setBrightness((int)SmartDashboard.getNumber("Brightness", 0));
            
            input.grabFrame(src);
//            Core.flip(src, dst, -1);
            output.putFrame(dst);
            
            Timer.delay(0.005);
        }
    }
}
