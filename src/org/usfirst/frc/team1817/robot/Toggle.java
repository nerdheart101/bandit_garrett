package org.usfirst.frc.team1817.robot;

public class Toggle {
    boolean tracker, output;
    Thread t;
    
    public Toggle(){
        tracker = false;
        output = false;
    }
    
    public void update(boolean input){
        if(input){
            if(!tracker){
                tracker = true;
                output = !output;
            }
        } else {
            tracker = false;
        }
    }
    
    public boolean toggled(){
        return output;
    }
    
    public void set(boolean value){
        output = value;
    }
}
