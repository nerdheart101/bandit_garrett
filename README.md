Controls

Driver:
Left Stick Y - Tank Left, Arcade Forward
Right Stick Y - Tank Right,
Right Stick X - Arcade Steer

Start - Toggle Tank/Arcade (Default)

RB - Shifter Low (Released)/High (Pressed)

A - Gear Down
B - Grabber Open/Close
X - Gear Score
Y - Gear Up (Default)

Manipulator:
RT - Intakes
LT - Climber

RB - Toggle Intake Up (Default)/Down

Y - Backshoot
A - Shoot
